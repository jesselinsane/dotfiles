# README #

Jesse's hobbyist programming repo.

## What is this repository for? ##

### Summary ###

This repo stores my personal Arch Linux configuration setup and scripts. 
This repo can be used and distributed according to copyleft principles as described in the Licensing section and in the COPYING file.

### Why? ###

I am strong supporter of the free and open source software (FOSS) movement and would encourage anyone who is not aware of what that is to read further here: https://www.gnu.org/philosophy/free-sw.html
In essence, software, like speech, ought to be free.
In an age of ramping cybersecurity threats and the interweaving of information technology into the heartbeat of our world, users from across the globe ought to have access to scrutinize, improve, and be inspired by all the source code that runs much of modern society.
Even for non-technical uses, which is the majority of my use cases, I have found Linux operating systems and their associated tools to be flexible, powerful, and easy-to-use for everyday tasks such as writing—and also an educational experience in both the realm of computer science and personal computing freedoms.
Thus, I would strongly recommend these OSes to desktop users looking to stand by free ideals in the new era of global information technology.

## Installation ##

I have not packaged these configurations properly for installation, nor do I ever plan to because these files are primarily for my personal use. However, I manage the majority of these files using GNU Stow so you may install them similarly, and perhaps some might find other uses in perusing them.

## Licensing ##

These programs are free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or at your option) any later version.

These programs distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
