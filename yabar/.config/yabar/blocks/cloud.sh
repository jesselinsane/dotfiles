#!/bin/bash
#insync_status=$(insync get_status)
#dropbox_status=$(dropbox-cli status | { read first rest ; echo $first ; }) 
#if [ $insync_status != "SYNCED" ]
#then
#    echo "$dropbox_status $insync_status"
#else
#    if [ $dropbox_status = "Up" ]
#    then
#        echo "Synced"
#    else
#        echo "$dropbox_status"
#    fi
#fi

# if you only care about dropbox
dropbox_status=$(dropbox-cli status | { read first rest ; echo $first ; }) 
case $dropbox_status in
# Up to date.
"Up")
	message="SYNCED"
	;;
# Syncing
"Syncing")
    message="SYNCING"
    ;;
# Connecting
"Connecting...")
	message="wait"
	;;
# Can't connect
"Can't")
	message="down"
	;;
# Dropbox isn't running!
"Dropbox")
	message="off"
	;;
*)
	message=$dropbox_status
esac

echo $message
