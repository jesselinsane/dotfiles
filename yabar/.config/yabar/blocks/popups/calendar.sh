#!/bin/sh
duration=5

barx=50
bary=50
barw=120
barh=20

bar_bg='#ff333333'
bar_fg='#ffffffff'

bar_font='xft:Terminus:size=12'

baropt='-g ${barw}x${barh}+${barx}+${bary} -B ${bar_bg} -F ${bar_fg}'

echo $(cal) | lemonbar -g ${barw}x${barh}+${barx}+${bary} -B ${bar_bg} -F ${bar_fg}
