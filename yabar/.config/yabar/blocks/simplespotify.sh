#/bin/bash
artist=$(playerctl metadata xesam:artist)
album=$(playerctl metadata xesam:album)
song=$(playerctl metadata xesam:title)
if [ -z "$artist" ] && [ -z "$album" ]; then
    echo "$song"
else
    echo ""$artist", "$album" — "$song""
fi
