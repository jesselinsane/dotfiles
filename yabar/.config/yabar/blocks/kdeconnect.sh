#!/bin/bash
paired=$(kdeconnect-cli -a | grep device | awk '{print $1}')
avail=$(kdeconnect-cli -l | grep device | awk '{print $1}')
if [ $avail = "1" ]
then
	case $paired in
	"0")
		status="n/a"
        kdeconnect-cli --pair -d dc72c353bacc580c &
		;;
	"1")
		status="S8"
		;;
	esac
else
	status="n/a"
fi

echo "$status"
