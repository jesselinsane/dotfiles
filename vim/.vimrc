syntax on
execute pathogen#infect()
let $PYTHONPATH='/usr/lib/python3.5/site-packages'
set laststatus=2
let g:powerline_pycmd = "py3"
let g:Powerline_colorscheme = 'base16'
set t_Co=256
"let g:solarized_termcolors=256
let base16colorspace = 256
colorscheme base16-ocean
set hlsearch
set number
set ignorecase
set smartcase
set incsearch
set tabstop=4
set softtabstop=4
set expandtab
nnoremap Y y$
" set cursor shapes by mode
let &t_SI = "\<Esc>[6 q"
let &t_SR = "\<Esc>[4 q"
let &t_EI = "\<Esc>[2 q"
