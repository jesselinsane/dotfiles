#!/bin/sh
cludgewrapper() {
#    killall compton
    "$@"
#    compton &
#    systemctl --user restart redshift
}
powermenu() {
case $1 in
    lock)
        xdotool key Escape
        pactl set-sink-mute 0 1
        playerctl pause
        killall -SIGUSR1 dunst
#        killall compton
#        i3lock -n -i /usr/share/backgrounds/ultrawide.png
#        pactl set-sink-mute 0 0
        light-locker-command -l
        killall -SIGUSR2 dunst
#        compton &
        ;;
    logout)
        bspc quit
        ;;
    suspend)
        xdotool key Escape; cludgewrapper systemctl suspend
        ;;
    hibernate)
        xdotool key Escape; cludgewrapper systemctl hibernate
        ;;
    reboot)
        xdotool key Escape; systemctl reboot
        ;;
    poweroff)
        xdotool key Escape; systemctl poweroff
        ;;
esac
}
powermenu "$@"
