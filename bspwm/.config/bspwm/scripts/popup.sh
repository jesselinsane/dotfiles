#!/bin/sh

# how long should the popup remain?
duration=3

# define geometry
barx=117
bary=60
barw=500
barh=500

# colors
bar_bg='#ff333333'
bar_fg='#ffffffff' # white is default

# font used
bar_font='-xos4-terminus-medium-r-normal-*-16-*-*-*-*-*-*-*'

# compute all this
baropt="-g ${barw}x${barh}+${barx}+${bary} -B${bar_bg} -f ${bar_font} -d"

#Create the popup and make it live for 3 seconds
( cal ; sleep ${duration}) | lemonbar ${baropt}
#echo " $@";
