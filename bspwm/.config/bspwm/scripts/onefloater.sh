#!/bin/bash

#wip a robust version
#floating_desktop_id=$(bspc query -D -d)
#
#if [ -e /tmp/$USER/floater${floating_desktop_id} ]
#then
#        kill $(cat /tmp/$USER/floater${floating_desktop_id})
#        rm /tmp/$USER/floater${floating_desktop_id}
#        exit 0
#else
#        ( bspc subscribe node_manage ; echo $! > /tmp/$USER/floater${floating_desktop_id} 2>/dev/null ) | while read -a msg
#        do
#                desk_id=${msg[2]}
#                wid=${msg[3]}
#                [ "$floating_desktop_id" = "$desk_id" ] && bspc node "$wid" -t floating
#        done
#fi 

#workable 1 workspace version

readarray -t floaters /tmp/$USER/floaters
floating_desktop_id=($(bspc query -D -d))

for i in "${floaters[@]}"
do
    if [ "$i" = "$floating_desktop_id" ]
    then
        floaters=( "${floaters[@]/$floating_desktop_id}" )
        grep -vF "$floating_desktop_id" /tmp/$USER/floaters | sponge /tmp/$USER/floaters
    else
        floaters=(${floaters[@]} $floating_desktop_id)
        echo $floating_desktop_id >> /tmp/$USER/floaters
    fi
done

if [ ${#floaters[@]} -eq 0 ]
then
        pkill -f "bspc subscribe node_manage" && exit 0
else
        bspc subscribe node_manage | while read -a msg
        do
                desk_id=${msg[2]}
                wid=${msg[3]}
                for i in "${floaters[@]}"
                do
                        if [ "$i" = "$desk_id" ]
                        then
                        bspc node "$wid" -t floating
                        fi
                done
        done
fi
