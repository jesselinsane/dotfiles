#!/bin/bash

awk '!seen[$0]++' /tmp/$USER/floaters | sponge /tmp/$USER/floaters
readarray -t floaters < /tmp/$USER/floaters
floating_desktop_id=($(bspc query -D -d))

if [ ${#floaters[@]} -eq 0 ]
then
        floaters=(${floaters[@]} $floating_desktop_id)
        echo $floating_desktop_id >> /tmp/$USER/floaters
else
        for i in "${floaters[@]}"
        do
            if [ "$i" = "$floating_desktop_id" ]
            then
#                floaters=( "${floaters[@]/$i}" )
                grep -vF "$i" /tmp/$USER/floaters | sponge /tmp/$USER/floaters
                break
            else
#                floaters=(${floaters[@]} $floating_desktop_id)
                echo $floating_desktop_id >> /tmp/$USER/floaters
            fi
        done
fi

awk '!seen[$0]++' /tmp/$USER/floaters | sponge /tmp/$USER/floaters
readarray -t floaters < /tmp/$USER/floaters
if [ ${#floaters[@]} -eq 0 ]
then
        pkill -f "bspc subscribe node_manage" &&
        exit 0
else
        pkill -f "bspc subscribe node_manage"
        bspc subscribe node_manage | while read -a msg
        do
                desk_id=${msg[2]}
                wid=${msg[3]}
                for i in "${floaters[@]}"
                do
                        if [ "$i" = "$desk_id" ]
                        then
                        bspc node "$wid" -t floating
                        fi
                done
        done
fi
