#!/bin/bash
# A fuzzy file-finder and opener based on dmenu
# Requires: dmenu, xdg-utils

eval $(xdotool getmouselocation --shell)
menu_width=600
monitor_width=$(wattr w $(lsw -r))
monitor_height=$(wattr h $(lsw -r))
lines=20
menu_height=$(( $lines * 23 ))
maxx=$(( $monitor_width - $menu_width ))
miny=16
maxy=$(( $monitor_height - $menu_height ))
XP=$X
[[ $XP -gt $maxx ]] && XP=$maxx
YP=$Y
[[ $YP -lt $miny ]] && YP=$miny
[[ $YP -gt $maxy ]] && YP=$maxy

locate ~/ | sed 's/ /\\ /g' | sort -f | dmenu -i -l $lines -fn 'terminus-12' -nb '#343d46' -nf '#a7adba' -sf '#c0c5ce' -sb '#bf616a' | xargs xdg-open
