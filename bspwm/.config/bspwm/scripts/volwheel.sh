#!/bin/sh
case $1 in
"up")
	if [ $(amixer sget Master | tail -1 | cut -d' ' -f8) = '[off]' ]
	then
		text="MUTE"
	else
		amixer -q set Master 2%+
		text=$(amixer sget Master | tail -1 | cut -d' ' -f7 | tr -d '[]%')
	fi
#	icon="/usr/share/icons/Paper/16x16/status/audio-volume-high.png"
	;;
"down")
	if [ $(amixer sget Master | tail -1 | cut -d' ' -f8) = '[off]' ]
	then
		text="MUTE"
	else
		amixer -q set Master 2%-
		text=$(amixer sget Master | tail -1 | cut -d' ' -f7 | tr -d '[]%')
	fi
#	icon="/usr/share/icons/Paper/16x16/status/audio-volume-low.png"
	;;
"mute")
	amixer -q set Master toggle
#	icon="/usr/share/icons/Paper/16x16/status/audio-volume-muted.png"
	if [ $(amixer sget Master | tail -1 | cut -d' ' -f8) = '[off]' ]
	then
		text="MUTE"
	else
		text=$(amixer sget Master | tail -1 | cut -d' ' -f7 | tr -d '[]%')
	fi
	;;
esac


id=$(cat /tmp/$USER/dunstvolume)
if [ $id -gt "0" ]
then
	dunstify -p -r $id -t 5 "Volume: $text" > /tmp/$USER/dunstvolume
else
	dunstify -p -t 5 "Volume: $text" > /tmp/$USER/dunstvolume
fi
