URxvt.font: xft:DejaVu Sans Mono:size=11
!!URxvt.font: -xos4-terminus-medium-r-normal-*-16-*-*-*-*-*-*-*

!! Font kerning fix for Source Code Pro and Monaco
URxvt.letterSpace: -1

!! Borders
URxvt.internalBorder:8
URxvt.externalBorder:0
URxvt.borderColor:#2b303b

!! clipboard settings
URxvt.iso14755 : false
!! URxvt.keysym.Shift-Control-c: perl:clipboard:copy
!! URxvt.keysym.Shift-Control-v: perl:clipboard:paste
!! URxvt.keysym.Shift-Control-a-v: perl:clipboard:paste_escaped

!! scrollbar 
URxvt.scrollBar: false

!! scrollback buffer in secondary screen
URxvt.secondaryScreen: 1
URxvt.secondaryScroll: 0

!! scrollback buffer size
URxvt.saveLines:65535 

!! Font hinting
Xft.autohint: 0
Xft.lcdfilter:  lcddefault
Xft.hintstyle:  hintslight
Xft.hinting: true
Xft.antialias: true
Xft.rgba: rgb
Xft.dpi: 96

!! base16 ocean dark
#define base00 #2b303b
#define base01 #343d46
#define base02 #4f5b66
#define base03 #65737e
#define base04 #a7adba
#define base05 #c0c5ce
#define base06 #dfe1e8
#define base07 #eff1f5
#define base08 #bf616a
#define base09 #d08770
#define base0A #ebcb8b
#define base0B #a3be8c
#define base0C #96b5b4
#define base0D #8fa1b3
#define base0E #b48ead
#define base0F #ab7967

*.foreground:   base05
*.background:   base00
*.cursorColor:  base05

*.color0:       base00
*.color1:       base08
*.color2:       base0B
*.color3:       base0A
*.color4:       base0D
*.color5:       base0E
*.color6:       base0C
*.color7:       base05

*.color8:       base03
*.color9:       base08
*.color10:      base0B
*.color11:      base0A
*.color12:      base0D
*.color13:      base0E
*.color14:      base0C
*.color15:      base07

! Note: colors beyond 15 might not be loaded (e.g., xterm, urxvt),
! use 'shell' template to set these if necessary
*.color16:      base09
*.color17:      base0F
*.color18:      base01
*.color19:      base02
*.color20:      base04
*.color21:      base06

!! solarized 
!#define S_base03        #002b36
!#define S_base02        #073642
!#define S_base01        #586e75
!#define S_base00        #657b83
!#define S_base0         #839496
!#define S_base1         #93a1a1
!#define S_base2         #eee8d5
!#define S_base3         #fdf6e3
!
!!! solarized light
!*background:            S_base3
!*foreground:            S_base00
!*fadeColor:             S_base3
!*cursorColor:           S_base01
!*pointerColorBackground:S_base1
!*pointerColorForeground:S_base01
!
!!! solarized dark
!!!*background:            S_base03
!!!*foreground:            S_base0
!!!*fadeColor:             S_base03
!!!*cursorColor:           S_base1
!!!*pointerColorBackground:S_base01
!!!*pointerColorForeground:S_base1
!
!#define S_yellow        #b58900
!#define S_orange        #cb4b16
!#define S_red           #dc322f
!#define S_magenta       #d33682
!#define S_violet        #6c71c4
!#define S_blue          #268bd2
!#define S_cyan          #2aa198
!#define S_green         #859900
!
!!! black dark/light
!*color0:                S_base02
!*color8:                S_base03
!
!!! red dark/light
!*color1:                S_red
!*color9:                S_orange
!
!!! green dark/light
!*color2:                S_green
!*color10:               S_base01
!
!!! yellow dark/light
!*color3:                S_yellow
!*color11:               S_base00
!
!!! blue dark/light
!*color4:                S_blue
!*color12:               S_base0
!
!!! magenta dark/light
!*color5:                S_magenta
!*color13:               S_violet
!
!!! cyan dark/light
!*color6:                S_cyan
!*color14:               S_base1
!
!!! white dark/light
!*color7:                S_base2
!*color15:               S_base3
!
!!! something from archlinux bbs that fixes solarized
!1URxvt.intensityStyles: false
! ------------------------------------------------------------------------------
! ROFI Color theme
! ------------------------------------------------------------------------------
rofi.color-enabled: true
rofi.color-window: #343d46, #96b5b4, #96b5b4
rofi.color-normal: #343d46, #c0c5ce, #4f5b66, #8fa1b3, #eff1f5
rofi.color-active: #343d46, #96b5b4, #4f5b66, #96b5b4, #eff1f5
rofi.color-urgent: #343d46, #bf616a, #4f5b66, #bf616a, #eff1f5
rofi.separator-style: solid
rofi.font: xos4 terminus 12

! keybindings
rofi.kb-clear-line: Control+u
rofi.kb-remove-to-eol:
rofi.kb-remove-to-sol:
rofi.kb-accept-entry: Control+m,Return,KP_Enter
rofi.kb-row-up: Control+k
rofi.kb-row-down: Control+j
rofi.kb-remove-word-back: Control+w,Control+BackSpace
rofi.kb-row-tab: Tab
rofi.kb-row-select: Control+space

