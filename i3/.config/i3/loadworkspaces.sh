#!/bin/bash
# loads Workspace 2: emacs/terminals
i3-msg "workspace 2; append_layout ~/.config/i3/layouts/workspace2.json"
# executes programs
(emacs &)
(urxvt &)
(urxvt &)
(urxvt &)
(urxvt &)

# loads Workspace 3: full-screen terminals
i3-msg "workspace 3; append_layout ~/.config/i3/layouts/workspace3.json"
# executes programs
(urxvt &)
(urxvt &)
(urxvt &)

# loads Workspace 9: communication
i3-msg "workspace 9; append_layout ~/.config/i3/layouts/workspace9.json"
## executes programs
(telegram-desktop &)
(discord-canary &)
#(skype &)
