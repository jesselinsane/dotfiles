#!/bin/bash
#insync_status=$(insync get_status)
#dropbox_status=$(dropbox-cli status | { read first rest ; echo $first ; }) 
#if [ $insync_status != "SYNCED" ]
#then
#    echo "$dropbox_status $insync_status"
#else
#    if [ $dropbox_status = "Up" ]
#    then
#        echo "Synced"
#    else
#        echo "$dropbox_status"
#    fi
#fi

# if you only care about dropbox
dropbox_status=$(dropbox-cli status | { read first rest ; echo $first ; }) 
if [ $dropbox_status = "Up" ]
then
    echo "SYNCED"
else
    echo "$dropbox_status"
fi
